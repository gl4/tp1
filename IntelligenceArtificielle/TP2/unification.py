class Unificateur:
    def __init__(self,unifie=[],unifiant=[]):
        self.unifie=unifie
        self.unifiant=unifiant


def unifier_atomes(E1,E2):
    #E1 atome ou E2
    if type(E2) is list:
        E2=E2[0]
    if type(E1) is list:
        E1=E1[0]
    if E1==E2:
        return None
    if E1[0]=='?':
        if E2.find(E1)!=-1:
            return False
            #E1 apparait dans E2
        u.unifie.append(E1)
        u.unifiant.append(E2)
        return u
    #si E1 constant et E2 variable
    if E2[0]=='?':
        u.unifie.append(E2)
        u.unifiant.append(E1)
        return u
    #le cas des fonctions
    if E2[0]==E1[0]:
        E11=list_des_atomes(E1,'f','g')
        E22=list_des_atomes(E2,'f','g')
        for i in range(0,len(E11)):
            Z11=unifier_atomes(E11[i],E22[i])
            appliquer_substitution(E11, E22,u)
        if E11==E22:
            return u
        else:
            return True
    return False


def list_des_atomes(E,P,Q):
    M=[]
    if E[0]==P or E[0]==Q:
        if E!='':
            for i in range(0,len(E)):
                if E[i]=='(':
                    for j in range(0, len(E)):
                        if E[-j]==')':
                            F=E[i + 1:-j]
                            break
                    break

#F=( contenu de f )+ enleve ()
            x=''
            p=0
            if len(F)!=1:
                for i in range(1,len(F)):
                    if F[i-1]!=',' and F[i]!='(' and p==0:
                        #p==0 car pour p >0 on a au sein dune fonction
                        x=x+F[i-1]
                    elif F[i-1]==',' and p==0:
                        M.append(x)
                        x=''
                    elif F[i]=='(' and p==0:
                        x=''
                        s=0
                        for j in F[i-1:]:
                            p=p+1
                            if j!=')' and j!='(':
                                x=x+j
                            elif j=='(':
                                x = x + j
                                s=s+1
                            elif j==')':
                                s=s-1
                                x=x+j
                                if s==0:
                                    M.append(x)
                                    x=''
                                    break
                    elif p!=0:
                        p=p-1
                        continue
                if x!='' or i==len(F)-1:
                    if F[len(F)-1]!=')':
                        M.append(x+F[len(F)-1])
            else:
                return F
        return M
    else:
        return E



def UNIFIER(E1,E2):
    E1=list_des_atomes(E1,'P','Q')
    E2=list_des_atomes(E2,'P','Q')
    if isinstance(E1,str) or (isinstance(E1,list) and len(E1)==1) or (isinstance(E2,str) and len(E2)==1):
        return unifier_atomes(E1,E2)
    F1=E1[0]
    F2=E2[0]
    Z1=UNIFIER(F1,F2)
    if Z1==False:
        return False
    if Z1!=None:
        appliquer_substitution(E1, E2,Z1)
    T1=E1[1:]
    T2 = E2[1:]
    Z2=UNIFIER(T1,T2)
    if Z2==False:
        return False
    return True


def appliquer_substitution(M1,M2,s):
     for j in range(0,len(s.unifiant)):
        for i in range(0, len(M1)):
            p = M1[i].find(s.unifie[j])
            if p != -1:
                if M1[i][0] != '?' or M1[i] == s.unifie[j]:
                    M1[i] = M1[i].replace(M1[i][p:p + len(s.unifie[j])],s.unifiant[j])

        for i in range(0, len(M2)):
            p = M2[i].find(s.unifie[j])
            if p != -1:
                if M2[i][0] != '?' or M2[i] == s.unifie[j]:
                    M2[i] = M2[i].replace(M2[i][p:p + len(s.unifie[j])],s.unifiant[j])


print("\t\t======== UNIFICATION ALGO =========")
print("\nUNIFIER(X,Y)")
X = input("\n X=? :")
Y = input("\n Y=? :")


u=Unificateur([],[])


p=UNIFIER(X,Y)
#   P(B,C,?x,?z,f(A,?z,B))        P(?y,?z,?y,C,?w)
#   P(?x,f(g(?x)),a)              P(b,?xy,?z)
#   Q(f(A,?x),?x)                 Q(f(?z,f(?z,D)),?z)
#   ?x                            g(?x)


if p==u or p==True:
    print("\t\t---------------UNIFIACTION TERMINE--------------")
    for i in range(0,len(u.unifie)):
        print(u.unifie[i]+"/"+u.unifiant[i]+'\n')
        
    print("\t\t------------------------------------------------")
if p==False:
    print('ECHEC')

with open("trace.txt","w") as file:
    for i in range(0,len(u.unifie)):
        file.writelines(u.unifie[i]+"/"+u.unifiant[i]+'\n')
    if p==False:
        file.writelines('\nECHEC')