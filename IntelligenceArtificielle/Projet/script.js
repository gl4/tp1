//Document READY
    $(function (){
        // Variables Globales 
        // Variable test click
        window.beginEnd = -1;
        window.found = false;
        window.trace = document.getElementById("trace");
        window.queue = [];
        window.chemin = [];
     // Variable de make   
    var $ = go.GraphObject.make;


    // Declaration WindowEdit :
        window.diagram =
        $(go.Diagram, "myDiagramDiv",
        { 
            initialContentAlignment: go.Spot.Center, // center Diagram contents
            layout: $(go.TreeLayout,
                    { angle: 0, layerSpacing: 100, sorting: go.TreeLayout.SortingAscending }),
            "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
            "clickCreatingTool.archetypeNodeData":  // a node data JavaScript object
                { key: "Node", color: "lightblue", shape: "Circle",  heur:"0" },
                "linkingTool.archetypeLinkData":
                {cout : "0" , color : "blue"}
        });


        // Node Template :
        diagram.nodeTemplate =
        $(go.Node, "Auto",
        { fromLinkable: true, toLinkable: true ,fromLinkableSelfNode: true, toLinkableSelfNode: true },
        $(go.Shape,
        new go.Binding("figure","shape").makeTwoWay(),  // use this kind of figure for the Shape
        new go.Binding("fill", "color")),
        $(go.Panel, "Table",
            { defaultAlignment: go.Spot.Center, margin: 4 },
            $(go.RowColumnDefinition, { column: 1, width: 4 }),
            $(go.TextBlock, {editable : true},
            { row: 0, column: 0, columnSpan: 2},
            { font: "bold 20pt sans-serif" },
            new go.Binding("text", "key").makeTwoWay()),
            $(go.TextBlock, "h() = ",
            { row: 1, column: 0 }),
            $(go.TextBlock,
            {editable: true},
            { row: 1, column: 2 },
            new go.Binding("text", "heur").makeTwoWay())
        )
        );

        // Link Template :
            diagram.linkTemplate =
            $(go.Link, { relinkableFrom: true, relinkableTo: true },
            $(go.Shape,{strokeWidth : 5}, new go.Binding("stroke", "color")), // shape.stroke = data.color
            $(go.Shape,{ toArrow: "Standard", fill: null, strokeWidth : 5}, new go.Binding("stroke", "color"))  ,// shape.stroke = data.color
            $(go.TextBlock,   { segmentOffset: new go.Point(0, 20) },                      // this is a Link label
            {editable:true}, new go.Binding("text", "cout").makeTwoWay())
        );
        
        // LISTENERS 
            diagram.addDiagramListener("ObjectSingleClicked",function (e){setBeginEnd(e)});
    
        window.diagram.model = new go.GraphLinksModel(  window.nodes ,window.links)
        window.diagram.toolManager.linkingTool.isEnabled = false;




    // Declaration WindowFinal :
        window.finalDiagram = 
        $(go.Diagram, "myFianalDiagram",
        { 
            initialContentAlignment: go.Spot.Center, // center Diagram contents
            layout: $(go.TreeLayout,
                    { angle: 0, layerSpacing: 100, sorting: go.TreeLayout.SortingAscending }),
            "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
        });


        // Node Template :
        finalDiagram.nodeTemplate =
        $(go.Node, "Auto",
        $(go.Shape,
        new go.Binding("figure","shape").makeTwoWay(),  // use this kind of figure for the Shape
        new go.Binding("fill", "color")),
        $(go.Panel, "Table",
            { defaultAlignment: go.Spot.Center, margin: 4 },
            $(go.RowColumnDefinition, { column: 1, width: 4 }),
            $(go.TextBlock,
            { row: 0, column: 0, columnSpan: 2},
            { font: "bold 20pt sans-serif" },
            new go.Binding("text", "key").makeTwoWay()),
            $(go.TextBlock, "h() = ",
            { row: 1, column: 0 }),
            $(go.TextBlock, { row: 1, column: 2 },
            new go.Binding("text", "heur"))
        )
        );

        // Link Template :
        finalDiagram.linkTemplate =
            $(go.Link, { relinkableFrom: true, relinkableTo: true },
            $(go.Shape,{strokeWidth : 5}, new go.Binding("stroke", "color")), // shape.stroke = data.color
            $(go.Shape,{ toArrow: "Standard", fill: null, strokeWidth : 5}, new go.Binding("stroke", "color"))  ,// shape.stroke = data.color
            $(go.TextBlock,   { segmentOffset: new go.Point(0, 20) },   // this is a Link label
            new go.Binding("text", "cout").makeTwoWay())
        );
        
        window.finalDiagram.model = new go.GraphLinksModel(  window.nodes ,window.links)
        window.finalDiagram.toolManager.linkingTool.isEnabled = false;
})