$.getJSON( "./nodes.json", function( data ) {
    window.nodes = data;
    })

$.getJSON( "./links.json", function( data ) {
    window.links = data
    })

// Début Fonction RESET
    function reset()
    {
        window.queue = [];
        window.chemin = [];
        window.trace.innerHTML = "";
        window.found = false;
        window.nodes.map(x => x.visited = false);
        window.nodes.map(x => x.color = "lightblue");
        window.links.map(x => x.color = "blue");
        var a = window.nodes.filter( x => x.status == "final");
        console.log(a);
        a.map( x => x.color = "lightgreen");
        a.map( x => x.shape = "Square");
        a = window.nodes.find( x=> x.status == "init");
        a.color = "orange";
        $("#step").removeClass("disabled");        
        window.finalDiagram.rebuildParts();
    }
// Fin Fonction RESET

// Fin Fonction modification etat des noeuds   
    function setBeginEnd(e)
    {
        var part = e.subject.part;
        if (!(part instanceof go.Link))
        {
            if (window.beginEnd == 0)
            {   
                if(window.diagram.model.nodeDataArray.find(x => x.status == "init"))
                {
                    var a = window.diagram.model.nodeDataArray.find(x => x.status == "init")
                    a.shape = "Circle";
                    a.status = "normal";
                    a.color = "lightblue";
                }
                part.data.shape = "Ring";
                part.data.status = "init";
                part.data.color = "orange";
                window.diagram.rebuildParts();
                window.beginEnd = -1;
            }
            
            if(window.beginEnd == 1)
            { 
                part.data.shape = "Square";
                part.data.status = "final";
                part.data.color = "lightgreen"
                window.diagram.rebuildParts();
                window.beginEnd = -1;
            }

            if(window.beginEnd == 2)
            {
                part.data.shape = "Circle";
                part.data.status = "normal";
                part.data.color = "lightblue";
                window.diagram.rebuildParts();
                window.beginEnd = -1;
            }
        } 
    }
// Fin Fonction modification etat des noeuds   

// Début Fonction (Créer Liens / Déplacer Noeuds )
    function swapLinkTool(b)
    {   
        var tool = window.diagram.toolManager.linkingTool;
         if(tool.isEnabled)
        {
        tool.isEnabled = false;
        b.textContent = "Ajouter Liens";
        }
        else 
        {
        tool.isEnabled = true;
        b.textContent = "Autoriser ajout liens";
        }
    }
// Fin Fonction (Créer Liens / Déplacer Noeuds )

// Début Fonction Générer Diagramme pour Parcours
    function generate()
    {
        window.nodes =  window.diagram.model.nodeDataArray;
        window.links = window.diagram.model.linkDataArray
        window.finalDiagram.model = new go.GraphLinksModel(window.nodes  , window.links);
    }
// Fin Fonction Générer Diagramme pour Parcours



// FONCTIONS DE PARCOURS \\ 



// Début A etoile
    function aEtoile()
    {
    var start = new Date().getTime();                
    window.trace.innerHTML = "Début de l'algorithme : A Etoile<br/>";        
    var s = window.nodes.find(x => x.status == "init");
    var fifo = []
    s.total = parseInt(s.heur);
    fifo.push(s)
    s.visited = true;
    while (fifo.length)
        {
            s = fifo.shift();
            window.trace.innerHTML += "On parcoure le noeud "+ s.key + " cout = " + s.total + "<br/>";                        
            console.log(s);
            window.queue.push(s);
            if (s.status == "final")
            {
                console.log("On a atteint le but " + s.key);
                window.trace.innerHTML += "On a atteint le but " + s.key +  "<br/>";                        
                window.found = true;
                var time = new Date().getTime() - start;
                window.trace.innerHTML += "Algoritme exécuté en " + time + " Millisecondes";
                return;
            }
            var voisins = links.filter( l => l.from == s.key);
            for ( i of voisins)
            {
                var newNode = nodes.find( l => l.key == i.to);
                newNode.total = parseInt(s.total) + parseInt(i.cout) + parseInt(newNode.heur);
               if (! newNode.visited )
                {   
                    newNode.parent = s.key;
                    newNode.visited = true;                    
                    fifo.push(newNode);
                }
            }
            fifo.sort(function comparer(a,b)
            {
                return a.total - b.total;
            });
        }
        var time = new Date().getTime() - start;
        window.trace.innerHTML += "Algorithme exécuté en " + time + " Millisecondes";    
    }
// Fin A etoile


// Début cout uniforme;
    function coutUniforme()
    {
    var start = new Date().getTime();                
    window.trace.innerHTML = "Début de l'algorithme : Cout Uniforme<br/>";        
    var s = window.nodes.find(x => x.status == "init");
    var fifo = []
    s.total = 0;
    fifo.push(s)
    s.visited = true;
    while (fifo.length)
        {
            s = fifo.shift();
            window.trace.innerHTML += "On parcoure le noeud "+ s.key + " cout = " + s.total + "<br/>";                        
            console.log(s);
            window.queue.push(s);
            if (s.status == "final")
            {
                console.log("On a atteint le but " + s.key);
                window.trace.innerHTML += "On a atteint le but " + s.key +  "<br/>";                        
                window.found = true;
                var time = new Date().getTime() - start;
                window.trace.innerHTML += "Algoritme exécuté en " + time + " Millisecondes";
                return;
            }
            var voisins = links.filter( l => l.from == s.key);
            for ( i of voisins)
            {
                var newNode = nodes.find( l => l.key == i.to);
                newNode.total = parseInt(s.total) + parseInt(i.cout);
               if (! newNode.visited )
                {   
                    newNode.parent = s.key;
                    newNode.visited = true;                    
                    fifo.push(newNode);
                }
            }
            fifo.sort(function comparer(a,b)
            {
                return a.total - b.total;
            });
        }
        var time = new Date().getTime() - start;
        window.trace.innerHTML += "Algorithme exécuté en " + time + " Millisecondes";    
    }
// Fin cout uniforme



// Début Fonctions Parcours Largeur
    function ParcoursLargeur()
    {
        var start = new Date().getTime();                
        window.trace.innerHTML = "Début de l'algorithme : Recherche par largeur (BFS)<br/>";        
        var s = window.nodes.find(x => x.status == "init");
        var fifo = []
        fifo.push(s)
        s.visited = true;
        while ( fifo.length)
            {
                s = fifo.shift();
                window.trace.innerHTML += "On parcoure le noeud "+ s.key + "<br/>";                        
                console.log(s);
                window.queue.push(s);
                if (s.status == "final")
                {
                    console.log("On a atteint le but " + s.key);
                    window.trace.innerHTML += "On a atteint le but " + s.key +  "<br/>";                        
                    window.found = true;
                    var time = new Date().getTime() - start;
                    window.trace.innerHTML += "Algoritme exécuté en " + time + " Millisecondes";
                    return;
                }
                var voisins = links.filter( l => l.from == s.key);
                for ( i of voisins)
                {
                    var newNode = nodes.find( l => l.key == i.to);
                   if (! newNode.visited )
                    {   
                        newNode.parent = s.key;
                        newNode.visited = true;                    
                        fifo.push(newNode);
                    }
                }
            }
            var time = new Date().getTime() - start;
            window.trace.innerHTML += "Algorithme exécuté en " + time + " Millisecondes";    
    }
// Fin Fonctions Parcours Largeur

// Début Fonctions Parcours Profondeur

    
    // Fonction Principale
    function parcoursProfondeur()
    {   
        window.trace.innerHTML = "Début de l'algorithme : Recherche par profondeur (DFS)<br/>";
        var start = new Date().getTime();        
        window.found = false ;
        window.nodes.map(x => x.visited = false );
        s = window.nodes.find( x => x.status == "init");
        explorerProfondeur(s,window.nodes,window.links);
        var time = new Date().getTime() - start;
        window.trace.innerHTML += "Algorithme exécuté en " + time + " Millisecondes";                                
    }

    // Fonction Récursive
    function explorerProfondeur(s,nodes,links)
    {   
        window.trace.innerHTML += "On parcoure le noeud "+ s.key + "<br/>";        
        console.log(s);
        window.queue.push(s);
        s.visited = true; 
        if (s.status == "final")
            {
                console.log("On a atteint le but " + s.key);
                window.trace.innerHTML += "On a atteint le but " + s.key +  "<br/>";                        
                window.found = true;
                return;
            }
        var voisins = links.filter( l => l.from == s.key);
        for (i of voisins)
            {   
                if (window.found)
                    return;
                var newNode = nodes.find( l => l.key == i.to);
                if (! newNode.visited )
                {   
                    newNode.parent = s.key;
                    explorerProfondeur(newNode  ,nodes,links);
                }
            }

    }
// Fin Fonctions Parcours Profondeur

// Affichage Function
    // Applicable 3al profondeur w largeur kahaw pour le moment
    function step()
    {   
        var a = window.queue[0];
        a.color = "lightyellow";
        window.queue.shift();
        if ( window.queue.length == 0)
        {
            $("#step").addClass("disabled");
        }
        window.finalDiagram.rebuildParts();        
    }
// Fin Fonction Affichage



// Génération du chemin pour le parcours
    // Applicable pour parcoursProfondeur pour le moment
    function generatePath()
    {
        var a = window.queue.find(x => x.status == "final");
        window.chemin.unshift(a);
        while( a.parent)
        {
            a = window.queue.find(x=> x.key == a.parent);
            window.chemin.unshift(a);
        }
        console.log(window.chemin);
    }
// Fin Génération du chemin

    // Fonctions d'enregistrement
    function saveNodesJSON()
     {
        $.ajax
        ({
            type: "GET",
            dataType : 'json',
            async: false,
            url: 'save_nodes.php',
            data: { data: JSON.stringify(window.nodes) },
            success: function () {alert("Thanks!"); },
            failure: function() {alert("Error!");}
        });
     }
     function saveLinksJSON()
     {
        $.ajax
        ({
            type: "GET",
            dataType : 'json',
            async: false,
            url: 'save_links.php',
            data: { data: JSON.stringify(window.links) },
            success: function () {alert("Thanks!"); },
            failure: function() {alert("Error!");}
        });
    }

    function saveTrace()
    {   
        var mytrace = window.trace.innerHTML.replace(/<br>/gi,"\n\r");
        console.log(mytrace);
        $.ajax
        ({
            type: "GET",
            dataType: "text",
            async: false,
            url: 'save_trace.php',
            data: { data: mytrace },
            success: function () {alert("Thanks!"); },
            failure: function() {alert("Error!");}
        });
    }