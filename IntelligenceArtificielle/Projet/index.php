<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <title>Bonjour</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="go.js"></script>
        <script src="jquery.min.js"></script>
        <script src="code.js"></script> 
        <script src="script.js"></script> 
        <script src="js/bootstrap.min.js"></script>
   </head>
   
    <body>
        <div id="myDiagramDiv" style="width:100%; height:500px; background-color: #DAE4E4;"></div>

        <button type="button" onclick="window.beginEnd = 0" class="btn btn-warning">Set Sommet</button>
        <button onclick="window.beginEnd = 1;" class="btn btn-success" >Set But</button>
        <button onclick="window.beginEnd = 2;" class="btn btn-primary" >Rendre Noeud Normal</button>             
        <button onclick="swapLinkTool(this);" class="btn btn-default">Ajouter des Liens</button>
        <button onclick="generate();" class="btn btn-success">Générer le diagramme final</button>
        <button onclick="step()" id="step" class="btn btn-warning">Etape</button>
        <button onclick="reset()" class="btn btn-danger">Reset</button>
        <div id="myFianalDiagram" style="width:100%; height:500px; background-color: #DAE4E4;"></div>
        <button onclick="parcoursProfondeur()" class="btn btn-primary">Parcours Profondeur</button>
        <button onclick="ParcoursLargeur()" class="btn btn-primary">Parcours Largeur</button>
        <button onclick="coutUniforme()" class="btn btn-primary">Parcours à cout uniforme</button>
        <button onclick="aEtoile()" class="btn btn-primary">A Etoile</button>

        <h2>Trace </h2>
        <p id="trace" style="width:500px;height:500px">
        </p>
    </body>
</html>