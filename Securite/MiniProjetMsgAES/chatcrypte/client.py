import socket
import threading
# Cryptage
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
backend = default_backend()
# Multiple de 16
def padd(string):
    c = " "
    length = len(string) // 16 + 1
    padding = c * (16 * length - len(string) )
    return padding + string

def unpad(string):
    c = " "
    i = 0 
    while i < len(string) and string[i] == c:
        i += 1
    return string[i:]

# Initialisation Socket :
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# Initialisation 
pseudo = socket.gethostname()     
port = 9999
host = "127.0.0.1"
host = input("Veuillez saisir l'adresse IP du serveur : ")

# connection au serveur et au port :
s.connect((host, port))                               
print("Vous êtes connectés en tant que : " + pseudo )

# Changer son pseudo :
choix = input("Voulez vous changer votre pseudo ? (O/N)")
choix = choix.lower()
if (choix == 'o') :
    pseudo = input("Veuillez donner votre Nouveau Pseudo : ")
    print("Votre pseudo est maintenant : " + pseudo)

# Declaration du Thread qui reçoit les messages du serveur
def reciever(socket,crypt):
    decrypt = crypt.decryptor()
    while True:
        try:
            resp = socket.recv(4096)
            resp = decrypt.update(resp) #+ decrypt.finalize()
            resp = str(resp.decode("utf-8"))
            print(unpad(resp))
        except:
            break
# Transformation de la méthode reciever en Thread
def client_thread(socket,crypt):
    thread = threading.Thread(target=reciever,args=(socket,crypt,))
    thread.daemon = True
    thread.start()

# Lancer le Thread
key = s.recv(32)
iv = s.recv(16)
cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)
client_thread(s,cipher)


encrypt = cipher.encryptor()

# Code d'envoi
while 1 :
    mymsg = input()
    if mymsg.lower() == 'q':
        break
    mymsg = str(pseudo + " : "  + mymsg)
    mymsg = padd(mymsg)
    mymsg = encrypt.update(mymsg.encode()) #+ encrypt.finalize()
    s.send(mymsg)

s.close()