#Serveur chat
import socket
import threading            

# Cryptage
import sys
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
backend = default_backend()

# Padd pour avoir des multiples de 16
def padd(string):
    c = " "
    length = len(string) // 16 + 1
    padding = c * (16 * length - len(string) )
    return padding + string
# unpad pour enlever les espaces
def unpad(string):
    c = " "
    i = 0 
    while i < len(string) and string[i] == c:
        i += 1
    return string[i:]


# Initialisation de l'Objet Socket
serversocket = socket.socket(
	        socket.AF_INET, socket.SOCK_STREAM) 
port = 9999
host = "127.0.0.1"                                  
host = input("Veuillez saisir l'adresse IP de votre machine :")

# Avoir acces au port
serversocket.bind((host, port))                                  

# Taille de la pile
serversocket.listen(5)

sockets = []

# La methode qui accepte les sockets client
def accept():
    while True :

        conn , addr = serversocket.accept()
        # Initialisation des clés Clients
        key = os.urandom(32)
        conn.send(key)
        iv2 = os.urandom(16)
        conn.send(iv2)
        # Initialisation du contexte
        ciphercl = Cipher(algorithms.AES(key), modes.CBC(iv2), backend=backend)
        encryptor = ciphercl.encryptor()
        decryptor = ciphercl.decryptor()
        cl = (conn,addr, encryptor, decryptor)
        sockets.append(cl)
        client_thread(cl)

# Thread de la methode accept
def accept_thread():
    thread = threading.Thread(target=accept)
    thread.daemon = True
    thread.start()    

# La methode qui gère la socket client
def client(c): # 0 conn , 1 adresse , 2 encryptor , 3 decryptor
    txt = padd("Vous etes bien connecte au serveur : ")
    encrypted = c[2].update(txt.encode())
    c[0].send(encrypted)
    while True:
        try:
            rec = c[0].recv(4096)
            decrypted = c[3].update(rec)
            rec = str(decrypted.decode())
            resp = str(c[1][0]) + ' => ' + unpad(rec)
            print(resp)
            sender(resp)
        except:
            raise
            break

# Le thread de la methode client
def client_thread(client_tup):
    thread = threading.Thread(target=client,args=(client_tup,))
    thread.daemon = True
    thread.start()   

# Methode de broadcast
def sender(msg):
    for tup in sockets:
        msg = padd(msg)
        encrypted = tup[2].update(msg.encode())
        tup[0].send(encrypted)

# Thread pour la methode sender
def send_thread(msg):
    thread = threading.Thread(target=sender,args=(msg,))
    thread.daemon = True
    thread.start()

accept_thread()
print("Le serveur de chat sécurisé a démarré")

while True:

    cmd = input()
    cmd = cmd.lower()
    if cmd == 'q':
        break

serversocket.close()