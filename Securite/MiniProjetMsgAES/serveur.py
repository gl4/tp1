import socket
import threading                                       

# Initialisation de l'Objet Socket
serversocket = socket.socket(
	        socket.AF_INET, socket.SOCK_STREAM) 
port = 9999                                           
host = input("Veuillez saisir l'adresse IP de votre machine : ")
approval = "Message bien recu du Serveur"
# bind to the port
serversocket.bind((host, port))                                  

# queue up to 5 requests
serversocket.listen(5)

sockets = []

# La methode qui accepte les sockets client
def accept():
    while True :
        conn,addr = serversocket.accept()
        cl = (conn,addr)
        sockets.append(cl)
        client_thread(cl)
# Thread de la methode accept
def accept_thread():
    thread = threading.Thread(target=accept)
    thread.daemon = True
    thread.start()    
# La methode qui gère la socket client
def client(c):
    c[0].send('Bien connecté au serveur !'.encode())
    while True:
        try:
            resp = str(c[1][0]) + ' => ' + c[0].recv(4096).decode()
            sender(resp)
            print(resp)
        except:
            raise
            break
# Le thread de la methode client
def client_thread(client_tup):
    thread = threading.Thread(target=client,args=(client_tup,))
    thread.daemon = True
    thread.start()   
# Methode de broadcast
def sender(msg):
    for tup in sockets:
        tup[0].send(msg.encode('utf-8'))
# Thread pour la methode sender
def send_thread(msg):
    thread = threading.Thread(target=sender,args=(msg,))
    thread.daemon = True
    thread.start()
# Methode qui reçoit les messages des clients
def reciever(clientsocket) :
    while True:                               
        msg =  clientsocket[0].recv(4096).decode('utf-8')
        print(msg)
        sender(msg)
# Thread pour la methode reciever
def recieve_thread(clientsocket):
    thread = threading.Thread(target=reciever,args=(clientsocket,))
    thread.daemon = True
    thread.start()



accept_thread()
print("Le serveur a bien démarré")

while True:

    cmd = input()
    cmd = cmd.lower()
    if cmd == 'q':
        break

serversocket.close()