import socket
import threading

# Initialisation Socket :
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# Initialisation 
pseudo = socket.gethostname()     
port = 9999
host = input("Veuillez saisir l'adresse IP du serveur : ")

# connection au serveur et au port :
s.connect((host, port))                               
print("Vous êtes connectés en tant que : " + pseudo )

# Changer son pseudo :
choix = input("Voulez vous changer votre pseudo ? (O/N)")
choix = choix.lower()
if (choix == 'o') :
    pseudo = input("Veuillez donner votre Nouveau Pseudo : ")
    print("Votre pseudo esst maintenant : " + pseudo)

# Declaration du Thread qui reçoit les messages du serveur
def reciever(socket):
    while True:
        try:
            resp = socket.recv(4096).decode('utf-8')
            print(resp)
        except:
            break
# Transformation de la méthode reciever en Thread
def client_thread(socket):
    thread = threading.Thread(target=reciever,args=(socket,))
    thread.daemon = True
    thread.start()

# Lancer le Thread
client_thread(s)

# Code d'envoi
while 1 :
    mymsg = input()
    if mymsg.lower() == 'q':
        break
    mymsg = pseudo + " : "  + mymsg
    s.send(mymsg.encode())

s.close()