import sys
import re

class Fait:

    def __init__(self, fait, explication = 0, negation = False):
        self.fait = fait
        self.explication = explication
        self.negation = negation

    def conflict(self, other_fait):
        if self.fait == other_fait.fait and not self.negation == other_fait.negation:
            return True
        return False

    def __eq__(self, other_fait):
        return other_fait.fait == self.fait and self.negation == other_fait.negation

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "{}{}".format("NON " if self.negation else "", self.fait)

class Regle:

    def __init__(self, name, permisse, conclusion):
        self.name = name
        self.permisses = permisse
        self.conclusions = conclusion
        self.active = True

    def applicable(self, base_fait):
        return all([x in base_fait for x in self.permisses])

    def __str__(self):
        return "{}: SI {} ALORS {}".format(self.name, ' ET '.join(map(str, self.permisses)), ' ET '.join(map(str, self.conclusions)))

class BaseFait:

    def __init__(self, file):
        self.faits = []
        self._parse_faits(file)

    def _parse_faits(self, file_fait):
        with open(file_fait) as f:
            for fait in f:
                negation = False
                if fait.startswith("NON"):
                    negation = True

                fait_name = fait.strip().split(' ')[-1]
                self.add(Fait(fait_name, negation = negation))


    def add(self, new_fait):
        for fait in self.faits:
            if fait.conflict(new_fait):
                print("Fait '{}' en confict avec '{}'".format(new_fait, fait))
                print("Error! exit.")
                sys.exit(1)
        if new_fait not in self.faits:
            self.faits.append(new_fait)
            return True

    def __contains__(self, fait):
        return fait in self.faits

    def __str__(self):
        output = ''
        for fait in self.faits:
            output += str(fait) + "\n"
        return output

class BaseRegle:

    def __init__(self, file):
        self.regles = []
        self._parse_regles(file)

    def _parse_regles(self, file_regles):
        with open(file_regles) as f:
            for regle_ligne in f:
                if regle_ligne.startswith("#"):
                    continue
                regle_ligne = regle_ligne.strip()
                regex = re.compile("(.+): SI (.+) ALORS (.+)")
                regle_name, permisse_raw, conclusion_raw = regex.match(regle_ligne).groups()
                permisse = self._parse_fait_like(permisse_raw)
                conclusion = self._parse_fait_like(conclusion_raw, True)
                self.regles.append(Regle(regle_name, permisse, conclusion))

    def _parse_fait_like(self, fait_like_raw, put_self = False):
        fait_like_list = list(map(lambda o: o.strip(), fait_like_raw.split('ET')))
        fait_like_obj = []
        for fait_like in fait_like_list:
            negation = False
            if fait_like.startswith("NON"):
                negation = True

            fait_like = fait_like.split(' ')[-1]
            fait_like_obj.append(Fait(fait_like, negation = negation))
        return fait_like_obj

    def regles_applicables(self, base_fait):
        applicables = []
        for regle in self.regles:
            if regle.active and regle.applicable(base_fait):
                applicables.append(regle)
        return applicables

    def __str__(self):
        output = ''
        for regle in self.regles:
            output += str(regle) + "\n"
        return output


class Moteur:

    def __init__(self, base_faits, base_regles):
        self.base_faits = base_faits
        self.base_regles = base_regles

    def chainage_avant(self, but, get_all = False, first = True):
        trace = []
        but = Fait(but)
        regle_applicables  = self.base_regles.regles_applicables(self.base_faits)
        while (but not in self.base_faits or get_all) and regle_applicables:
            regle = self.choix_regle(regle_applicables, first)
            print("regle :")
            print(regle)
            trace.append(regle)
            for conclusion in regle.conclusions:
                conclusion.explication = regle
                self.base_faits.add(conclusion)
            regle.active = False
            regle_applicables  = self.base_regles.regles_applicables(self.base_faits)

        return but in self.base_faits, trace

    def choix_regle(self, applicables, first = True):
        if first:
            return applicables[0]
        else:
            return max(applicables, key=lambda regle: len(regle.permisses))


def save_trace(traces):

    with open('trace.txt', 'w') as f:
        for trace in traces:
            f.write(str(trace))

def main():
    base_regles = BaseRegle(input("Donner le fichier de la base des regles : "))
    base_faits = BaseFait(input("Donner le fichier de la base des faits : "))
    but= input("Donner le but : ")
    print("REGLES :")
    print(base_regles)
    print("Faits :")
    print(base_faits)
    print
    moteur = Moteur(base_faits, base_regles)

    sature_base_fait = input("Voulez vous que la base soit Saturée (o/n) ? ")
    if sature_base_fait == 'o' :
        sature_base_fait = True
    else :
        sature_base_fait = False
    choix_premier_regle = True
    res, trace = moteur.chainage_avant(but, sature_base_fait, choix_premier_regle)
    print("*****************************")
    print("LA BASE DE FAITS")
    print(base_faits)
    print("*****************************")
    print("La TRACE de raisonnement :")
    print('\n'.join(map(str,trace)))
    print("*****************************")

    x = 'n'
    if res:
        print("BUT Atteint")
        x = input("Sauvegarder trace ?")
    else : print(but," est non atteint")
    if x == 'o':
        save_trace(trace)

main()